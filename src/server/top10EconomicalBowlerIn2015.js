const csvjson = require("csvjson");
const fs = require("fs");
const path = require("path");
const data = fs.readFileSync(path.join(__dirname,'../','data/deliveries.csv'), { encoding : 'utf8'});
let countsOfMatchs = require("./matchPlayedPerYear");
let convertedDataInArray = csvjson.toArray(data);
let arrOfDeleveriesIn2016 =[];
let listOfBowlers = [];
let searchId = (countsOfMatchs, year) => {
let id = 0;

      for (let index = 0; index < year - 2008 + 1; index++) {
            id += countsOfMatchs[index][1];
      }
      return id;
      };

let startID2015 = searchId(countsOfMatchs, 2015) + 1;
let startID2016 = searchId(countsOfMatchs, 2016);
let startIndexInConvertedData2015 = indexFinder(startID2015);
let endIndexInConvertedData2015 = indexFinder(startID2016);
let bowlerStats = [];

      function indexFinder(indexOf1stMatch){
            let startingIndex =0
            for (let index = 0; index < convertedDataInArray.length; index++) {
                  if (convertedDataInArray[index][0] == indexOf1stMatch) {
                        startingIndex = index;
                        break;
                  } 
            }
      return startingIndex+1;
      }

convertedDataInArray = convertedDataInArray.slice(startIndexInConvertedData2015, endIndexInConvertedData2015-1);


      for (let index = 0; index < convertedDataInArray.length; index++) {

            listOfBowlers.push(convertedDataInArray[index][8])
            arrOfDeleveriesIn2016.push({id: convertedDataInArray[index][0], over: convertedDataInArray[index][4], bowler:convertedDataInArray[index][8], runs: convertedDataInArray[index][17]})
      }
let setOfBowlers = new Set(listOfBowlers)
listOfbowlers = Array.from(setOfBowlers)

      for (let index = 0; index < listOfbowlers.length; index++) {
            let bowler = listOfbowlers[index];
            let count = 0;
            let deliveries = 0;
            for (let index1 = 0; index1 < arrOfDeleveriesIn2016.length; index1++) {
                  if(bowler == arrOfDeleveriesIn2016[index1].bowler)
                  {
                        deliveries++;
                        count+=parseInt(arrOfDeleveriesIn2016[index1].runs)
                  }
            }
      bowlerStats.push([bowler,((count/deliveries)*6).toFixed(2)])
      }

bowlerStats = bowlerStats.sort(function(a, b) {
      return a[1] - b[1];
}).slice(0,10)



module.exports = bowlerStats;
