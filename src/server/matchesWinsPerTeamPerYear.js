const csvjson = require("csvjson");                                                     
const fs = require("fs");
const path = require("path");
const data = fs.readFileSync(path.join(__dirname,'../','data/matches.csv'), { encoding : 'utf8'});

let convertedDataToArray = csvjson.toArray(data);
let countsOfMatchs = require("./matchPlayedPerYear.js");
let arrOfObjOfWinner = [];
let arrSeperatedYearRecords = [];
let arrOfObjectsOfTeamWinsWithYear= [];

convertedDataToArray.shift();
convertedDataToArray = convertedDataToArray
  .slice(countsOfMatchs[9][1], convertedDataToArray.length)
  .concat(convertedDataToArray.slice(0, countsOfMatchs[9][1]));

for (let index = 0; index < convertedDataToArray.length; index++) {
  arrOfObjOfWinner.push(
    convertedDataToArray[index][10]
  );
}// list of winners

for (let index = 0; index < 10; index++) {
  arrSeperatedYearRecords.push(
    arrOfObjOfWinner.slice(
    startpoint(index, countsOfMatchs),
    startpoint(index, countsOfMatchs) + countsOfMatchs[index][1]
    )
  );
}//list of winners distributed as per year

for (let index = 0; index < arrSeperatedYearRecords.length; index++) {
    arrOfObjectsOfTeamWinsWithYear.push({year: 2008+index, wins:countingWins(arrSeperatedYearRecords[index])})
    
}

function startpoint(index, ans) {
  
  let start = 0;
  if (index == 0) {
    return 0;
  } else {
    for (let ind = 0; ind < index; ind++) {
      start += ans[ind][1];
    }
    return start;
  }
}
function countingWins(passedarr)
{
const counts = {};
passedarr.forEach((x) => {
  counts[x] = (counts[x] || 0) + 1;
});
return counts;
}



module.exports = arrOfObjectsOfTeamWinsWithYear;