let fs = require('fs');
const path = require("path");

let matchesPerYear = require('./matchPlayedPerYear')

let numberOfMatchesPerTeamPerYear = require('./matchesWinsPerTeamPerYear')

let extraRunsConcededByEachTeamIn2016 = require('./extraRunsPerTeamIn2016')

let top10EconomicalBowlerIn2015 = require('./top10EconomicalBowlerIn2015')


matchesPerYear = JSON.stringify(matchesPerYear)
numberOfMatchesPerTeamPerYear = JSON.stringify(numberOfMatchesPerTeamPerYear)
extraRunsConcededByEachTeamIn2016 = JSON.stringify(extraRunsConcededByEachTeamIn2016)
top10EconomicalBowlerIn2015 = JSON.stringify(top10EconomicalBowlerIn2015)


fs.writeFile(path.join(__dirname,'../','public/output'+ '/matchesPerYear.json'), matchesPerYear, 'utf8',function(err){console.log("matchesPlayedPerYear.json data saved to output");});
fs.writeFile(path.join(__dirname,'../','public/output'+ '/numberOfMatchesPerTeamPerYear.json'), numberOfMatchesPerTeamPerYear, 'utf8',function(err){console.log("numberOfMatchesPerTeamPerYear.json data saved to output");});
fs.writeFile(path.join(__dirname,'../','public/output'+ '/extraRunsConcededByEachTeamIn2016.json'), extraRunsConcededByEachTeamIn2016, 'utf8',function(err){console.log("extraRunsConcededByEachTeamIn2016.json data saved to output");});
fs.writeFile(path.join(__dirname,'../','public/output'+ '/top10EconomicalBowlerIn2015.json'), top10EconomicalBowlerIn2015, 'utf8',function(err){console.log("top10EconomicalBowlerIn2015.json data saved to output");});

