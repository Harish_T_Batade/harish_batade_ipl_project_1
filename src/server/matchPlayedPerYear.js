const csvjson = require("csvjson");
const fs = require("fs");
const path = require("path");
const data = fs.readFileSync(path.join(__dirname,'../','data/matches.csv'), { encoding : 'utf8'});
const convertedDataToArrOfObjects = csvjson.toObject(data); //converted data to arr of objects

let records = Array(10).fill(0); // Array to save the results

for (let index = 0; index < convertedDataToArrOfObjects.length; index++) {
  records[convertedDataToArrOfObjects[index].season - 2008]++;
} //for loop to count the no. of match each year
let recordsWithYear = [];
for (let index = 0; index < records.length; index++) {
    
    recordsWithYear.push([index+2008,records[index]])
} // for loop to convert records into 2D array in which every subarray's first element = year, secound element = count of matches that year

module.exports = recordsWithYear;

