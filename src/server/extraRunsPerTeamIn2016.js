const csvjson = require("csvjson");
const fs = require("fs");
const path = require("path");
const recordsWithYear = require("./matchPlayedPerYear");
const data = fs.readFileSync(path.join(__dirname,'../','data/deliveries.csv'), { encoding : 'utf8'});

let countsOfMatches = require('./matchPlayedPerYear')
let convertedDataInArray = csvjson.toArray(data);

let searchId = (countsOfMatchs) => {
    let count = 0;
    for (let index = 0; index < 2016-2008+1; index++) {
        
        count += countsOfMatchs[index][1]
    }
    return count
}

let firstMatchIDForYear2016 = searchId(countsOfMatches)

let startIndexInConvertedDataArray = 0;
for (let index = 0; index < convertedDataInArray.length; index++) {
    if(convertedDataInArray[index][0]==firstMatchIDForYear2016)
    {
        startIndexInConvertedDataArray = index;
        break;
    }
    
}
let endIndexInConvertedDataArray = 0;

for (let index = 0; index < convertedDataInArray.length; index++) {
    if(convertedDataInArray[index][0] == firstMatchIDForYear2016+countsOfMatches[8][1]-1)
    {
       
        endIndexInConvertedDataArray = index;
    }
}

convertedDataInArray = convertedDataInArray.slice(startIndexInConvertedDataArray,endIndexInConvertedDataArray+1);

let arrOfDeleveriesIn2016withExtraRun =[];
let listOfteams = [];
for (let index = 0; index < convertedDataInArray.length; index++) {
    if(convertedDataInArray[index][16]>0)
    {
        listOfteams.push(convertedDataInArray[index][3])
        arrOfDeleveriesIn2016withExtraRun.push({id: convertedDataInArray[index][0], over: convertedDataInArray[index][4], ball: convertedDataInArray[index][5] ,bowling:convertedDataInArray[index][3], extraruns: convertedDataInArray[index][16]})
    }
    
}
let setOfTeams = new Set(listOfteams)
listOfteams = Array.from(setOfTeams)

let recordsFor2016OfExtraRuns = [];
for (let index0 = 0; index0 < listOfteams.length; index0++) {
    let team = listOfteams[index0];
    let runs = 0;
    for (let index1 = 0; index1 < arrOfDeleveriesIn2016withExtraRun.length; index1++) {
        if(team == arrOfDeleveriesIn2016withExtraRun[index1].bowling)
        {
            runs+=parseInt(arrOfDeleveriesIn2016withExtraRun[index1].extraruns)
        }
    }
    recordsFor2016OfExtraRuns.push({team: team, extraruns: runs})
}



module.exports = recordsFor2016OfExtraRuns;